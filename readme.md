# Royale Tumble

Royale Tumble repository.

#### Installation
1. [Download Godot 2.1](https://godotengine.org/download)
3. Open Godot, *it should auto install*
4. Clone this repository anywhere: `git clone https://gitlab.com/grgp/royale-tumble.git`
5. On Godot, choose Import > <directory of cloned repository>

#### Quickstart tips
- Click the Play (►) button to run the game.
- `.tscn` files are called scenes, they're basically objects. They can be anything from a character to a level.
- `.gd` files are scripts.
- That's it really.

#### Essential tutorials
- [Intro to the Godot game engine
](https://www.youtube.com/watch?v=eHtIcbrii2Y&t=2s) [15 mins]
- [Physics in Godot](http://docs.godotengine.org/en/latest/learning/features/physics/physics_introduction.html#collisionobject2d)
- [Introduction to Vectors in Godot](https://www.youtube.com/watch?v=ZoMmiQes_lE) [8 mins]

##### Quick editor tips
- Hold the middle mouse button to move. (or just use the scrollbars).
- Layout is very similar to Unreal, except the file browser is on the left.
- Look around the inspector.

##### Quick scripting tips
- It's similar to Python.
- `_ready()` will get called only at the beginning.
- `_fixed_process(delta)` will get called constantly (every frame to be precise)

##### Optional tutorials
- [Godot tutorial: Horizontal platform movement
](https://www.youtube.com/watch?v=bSj7l25bdJ4) [20 mins]