extends KinematicBody2D

var direction = Vector2()

var speed = 0
const MAX_SPEED = 400

var velocity = Vector2()
export var input_button = "move_p1"


enum STATES {IDLE, WALKING}
var state = IDLE


func _ready():
	set_fixed_process(true)

func _fixed_process(delta):
	direction = Vector2()

	if Input.is_action_pressed(input_button):
		direction.x = -cos(get_rot())
		direction.y = sin(get_rot())

	if direction != Vector2():
		speed = MAX_SPEED
		state = WALKING
	else:
		speed = 0
		state = IDLE

	velocity = speed * direction.normalized() * delta

	if state == IDLE:
		rotate(0.02)
	else:
		move(velocity)